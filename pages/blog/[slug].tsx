import React from 'react'
import { SEO } from 'src/components/SEO'
import { TagLabel } from 'src/components/TagLabel'
import { Markdown } from 'src/components/Markdown'
import { api, BlogPage } from 'src/lib/api'
import { GetStaticProps } from 'next'
import { nextUtils } from 'src/lib/utils'
import { Link } from 'src/components/Link'

type BlogPostProps = {
  post: BlogPage
}

const BlogPost = ({ post }: BlogPostProps) => (
  <>
    <SEO title={post.data.title} description={post.data.summary} images={[post.data.mainImage.url]} />
    <main>
      <div className="grid py-12 mx-auto">
        <div className="mx-auto">
          <ul className="grid grid-flow-col gap-2">
            <Link href={'/blog'}>
              <TagLabel id={'Back to Blog posts'} />
            </Link>
          </ul>
        </div>

        <div className="pb-2">
          <h1 className="py-4 font-serif text-5xl font-bold text-center text-transparent md:text-6xl from-titleTextStart to-titleTextEnd bg-gradient-to-r bg-clip-text">
            {post.data.title}
          </h1>
        </div>

        <div className="mx-auto">
          <ul className="inline-flex flex-wrap gap-2">
            {post.data.tags.map((tag) => (
              <TagLabel key={tag} id={tag} />
            ))}
          </ul>
        </div>

        <br />

        <div className="mx-auto py text-textScript">
          <Markdown mdString={post.markdownContent} />
        </div>
      </div>
    </main>
  </>
)

export const getStaticPaths = async () => ({
  paths: api.cms.listBlogPosts().map((post) => ({
    params: { slug: post.slug },
  })),
  fallback: false,
})

export const getStaticProps: GetStaticProps<BlogPostProps> = async (context) => ({
  props: { post: api.cms.getBlogPost(nextUtils.getSlugFromStaticPropsContext(context)) },
})

export default BlogPost
