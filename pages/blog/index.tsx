import React from 'react'
import { SEO } from 'src/components/SEO'
import { BlogPostCard } from 'src/components/BlogPostCard'
import { api, BlogPage } from 'src/lib/api'
import { GetStaticProps } from 'next'

type BlogHomeProps = {
  allBlogs: BlogPage[]
}

const BlogHome = ({ allBlogs }: BlogHomeProps) => (
  <>
    <SEO title="Rachel Rogers | Blog" description="words words words, by rachel." />
    <main>
      <div className="grid py-12 mx-auto">
        <div className="pb-2">
          <h1 className="py-4 font-serif text-5xl font-bold text-center text-transparent md:text-6xl from-titleTextStart to-titleTextEnd bg-gradient-to-r bg-clip-text">
            Blog
          </h1>
          <p className="text-2xl font-bold text-center text-textScript">words words words, by rachel.</p>
        </div>

        <div className="mx-auto py">
          {allBlogs.map((post) => (
            <BlogPostCard
              key={post.slug}
              title={post.data.title}
              slug={post.slug}
              description={post.data.summary}
              date={post.data.publishDate}
              coverImageUrl={post.data.mainImage.url}
              coverImageAlt={'TODO!'}
            />
          ))}
        </div>
      </div>
    </main>
  </>
)

export const getStaticProps: GetStaticProps<BlogHomeProps> = async () => ({
  props: {
    allBlogs: api.cms.listBlogPosts(),
  },
})

export default BlogHome
