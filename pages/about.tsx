import React from 'react'
import { GetStaticProps } from 'next'
import { SEO } from 'src/components/SEO'
import { Image, ImageRepository } from 'src/components/Image'
import { dateUtils } from 'src/lib/utils'
import { api } from 'src/lib/api'

type AboutProps = {
  title: string
  content: string
}

const About = ({ title, content }: AboutProps) => {
  const tagline = `I'm a ${dateUtils.numYearsSince(1994)} year-old self-taught photographer based in NYC.`

  return (
    <>
      <SEO title={'About'} description={tagline} />
      <main>
        <div className="grid gap-8 justify-items-center">
          <Image className="rounded-full" src={ImageRepository.InstagramProfile} alt={'Rachel Rogers Profile'} />
          <h1 className="pb-8 font-serif text-5xl font-bold text-center text-transparent md:text-6xl from-titleTextStart to-titleTextEnd bg-gradient-to-r bg-clip-text">
            Rachel R.
          </h1>
        </div>

        <div className="w-10/12 mx-auto">
          <h2 className="py-6 font-serif text-2xl font-bold text-textScript">{title}</h2>

          <div className="max-w-full prose prose-lg text-textScript">
            {content
              .split('\n')
              .filter((section) => !!section)
              .map((section) => section.trim())
              .filter((section) => section !== '')
              .map((section) => (
                <p key={Math.random()}>{section}</p>
              ))}
          </div>
        </div>
      </main>
    </>
  )
}

export const getStaticProps: GetStaticProps<AboutProps> = async () => ({
  props: {
    title: api.cms.getSiteContent().aboutTitle,
    content: api.cms.getSiteContent().aboutContent,
  },
})

export default About
