import React from 'react'
import { NextPage } from 'next'

const Contact: NextPage = () => (
  <>
    <form
      className="w-full max-w-lg mx-auto"
      name="contact"
      method="POST"
      action="/contact-submitted"
      data-netlify="true"
    >
      <input type="hidden" name="form-name" value="contact" />
      <div className="flex flex-wrap -mx-3 mb-6">
        <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
          <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="first-name">
            First Name
          </label>
          <input
            className="appearance-none block w-full bg-gray-200 text-gray-700 border border-red-500 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
            id="first-name"
            name="first-name"
            type="text"
            placeholder="Jane"
          />
          <p className="text-red-500 text-xs italic">Required.</p>
        </div>

        <div className="w-full md:w-1/2 px-3">
          <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="last-name">
            Last Name
          </label>
          <input
            className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
            id="last-name"
            name="last-name"
            type="text"
            placeholder="Doe"
          />
          <p className="text-gray-600 text-xs italic">Recommended.</p>
        </div>
      </div>

      <div className="flex flex-wrap -mx-3 mb-6">
        <div className="w-full px-3">
          <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="email">
            E-mail
          </label>
          <input
            className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
            id="email"
            name="email"
            type="email"
            placeholder="JaneDoe@website.com"
          />
          <p className="text-gray-600 text-xs italic">Recommended so I can reach back out to you.</p>
        </div>
      </div>

      <div className="flex flex-wrap -mx-3 mb-6">
        <div className="w-full px-3">
          <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="message">
            Message
          </label>
          <textarea
            className="no-resize appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500 h-48 resize-none"
            id="message"
            name="message"
          />
          <p className="text-gray-600 text-xs italic">
            Enter your message with contact information and hit {'"'}Send{'"'} below.
          </p>
        </div>
      </div>

      <div className="md:flex md:items-center">
        <div className="md:w-1/3">
          <button
            className="shadow bg-complement-400 hover:bg-complement-200 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded"
            type="submit"
          >
            Send
          </button>
        </div>
        <div className="md:w-2/3" />
      </div>
    </form>
  </>
)

export default Contact
