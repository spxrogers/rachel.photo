import React from 'react'
import { GetStaticProps } from 'next'
import { api } from 'src/lib/api'
import { ScrollingGallery } from 'src/components/ScrollingGallery'

type ArchiveProps = {
  images: GalleryGridImage[]
  serveGalleriesFromCdn: boolean
}

type GalleryGridImage = {
  url: string
  width: number
  height: number
}

const Archive = ({ images, serveGalleriesFromCdn }: ArchiveProps) => (
  <div className="w-full">
    <h1 className="py-4 font-serif text-5xl font-bold text-center text-transparent md:text-6xl from-titleTextStart to-titleTextEnd bg-gradient-to-r bg-clip-text">
      Archive
    </h1>

    <div className="pb-2">
      <p className="text-xl font-bold text-center text-textScript">:)</p>
    </div>

    <div className="grid items-center grid-flow-row grid-cols-1 gap-32 p-8 justify-items-center md:p-32">
      <ScrollingGallery
        images={images.map(({ url, height, width }) => ({
          url,
          width,
          height,
          title: 'TODO',
        }))}
        lazyLoadImages={true}
        serveGalleryFromCdn={serveGalleriesFromCdn}
      />
    </div>
  </div>
)

export const getStaticProps: GetStaticProps<ArchiveProps> = async () => ({
  props: {
    images: api.cms.getAllImages().map(({ url, height, width }) => ({
      url,
      width,
      height,
    })),
    serveGalleriesFromCdn: api.cms.getSiteContent().serveGalleriesFromCdn,
  },
})

export default Archive
