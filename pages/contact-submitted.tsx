import React from 'react'
import { NextPage } from 'next'
import { Link } from 'src/components/Link'
import { TagLabel } from 'src/components/TagLabel'

const ContactSubmitted: NextPage = () => (
  <>
    <main>
      <div className="grid py-12 mx-auto">
        <div className="pb-2">
          <h1 className="py-4 font-serif text-5xl font-bold text-center text-transparent md:text-6xl from-titleTextStart to-titleTextEnd bg-gradient-to-r bg-clip-text">
            Contact form submitted
          </h1>
        </div>

        <div className="pb-12">
          <p className="text-l font-bold text-center text-textScript font-mono">
            If needed, I will reach back out to you with the contact information provided.
          </p>
        </div>

        <div className="pb-2">
          <p className="text-xl font-bold text-center text-textScript">Thank you!</p>
        </div>

        <div className="mx-auto">
          <ul className="grid grid-flow-col gap-2">
            <Link href={'/'}>
              <TagLabel id={'Back to Home page'} />
            </Link>
          </ul>
        </div>
      </div>
    </main>
  </>
)

export default ContactSubmitted
