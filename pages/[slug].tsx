// rachel.photo/[slug] is an alias for rachel.photo/gallery/[slug]

import GalleryDetailPage from './gallery/[slug]'
export default GalleryDetailPage

export { getStaticPaths, getStaticProps } from './gallery/[slug]'
