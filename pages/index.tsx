import React from 'react'
import { GetStaticProps } from 'next'
import { GalleryCard, GalleryCardImage } from 'src/components/GalleryCard'
import { api } from 'src/lib/api'

type GallerySummary = {
  slug: string
  title: string
  year: number
  image: GalleryCardImage
  tags: string[]
}

type HomeProps = {
  galleries: GallerySummary[]
  paragraph: string
}

const Home = ({ galleries, paragraph }: HomeProps) => (
  <div className="py-24 mx-auto grid gap-20 md:gap-30">
    <div className="pb-8">
      <h1 className="pb-8 font-serif text-5xl font-bold text-center text-transparent md:text-6xl from-titleTextStart to-titleTextEnd bg-gradient-to-r bg-clip-text">
        Living a
        <br />
        Wonderfluff life
      </h1>
      <h2 className="pb-8 font-serif text-3xl font-bold text-center text-transparent md:text-4xl from-titleTextStart to-titleTextEnd bg-gradient-to-r bg-clip-text">
        Rachel Rogers
      </h2>
      <div className="py-8 mx-auto text-center prose prose-2xl text-textScript">
        <p>
          {paragraph}
          <br />
        </p>
      </div>
    </div>
    <div className="items-center grid grid-cols-1 justify-items-center gap-12 md:gap-48">
      {galleries.map((gallery) => (
        <GalleryCard
          key={gallery.slug}
          slug={gallery.slug}
          title={gallery.title}
          year={gallery.year}
          image={gallery.image}
          tags={gallery.tags}
          smallBackgroundImageStack={false}
        />
      ))}
    </div>
  </div>
)

export const getStaticProps: GetStaticProps<HomeProps> = async () => ({
  props: {
    galleries: api.cms
      .listGalleries()
      .filter((rawGallery) => rawGallery.slug !== api.cms.constant.FeaturedGallery)
      .map((gallery) => ({
        slug: gallery.slug,
        title: gallery.data.title,
        year: new Date(gallery.data.dateOfWork).getFullYear(),
        image: {
          fileName: 'TODO',
          url: gallery.data.images[0].url,
          height: gallery.data.images[0].height,
          width: gallery.data.images[0].width,
        },
        tags: gallery.data.tags,
      })),
    paragraph: api.cms.getSiteContent().homePageParagraph,
  },
})

export default Home
