import 'styles/globals.css'
import React from 'react'
import { AppProps } from 'next/app'
import Head from 'next/head'
import { Footer } from 'src/components/Footer'
import { Header } from 'src/components/Header'
import { SEO } from 'src/components/SEO'

const App = ({ Component, pageProps }: AppProps) => (
  <div>
    <Head>
      <meta content="width=device-width, initial-scale=1" name="viewport" />
    </Head>
    <SEO.Default />
    <div className="px-6 pb-6 mx-auto max-w-7xl">
      <Header />
      <div className="py-12 mx-auto max-w-7xl">
        <Component {...pageProps} />
      </div>
      <Footer />
    </div>
  </div>
)

export default App
