import React from 'react'
import { GetStaticProps } from 'next'
import { api, GalleryPage } from 'src/lib/api'
import { dateUtils, nextUtils } from 'src/lib/utils'
import { SEO } from 'src/components/SEO'
import { TagLabel } from 'src/components/TagLabel'
import { ScrollingGallery } from 'src/components/ScrollingGallery'
import { NBSP } from 'src/components/NBSP'

type GalleryDetailPageProps = {
  gallery: GalleryPage
  serveGalleriesFromCdn: boolean
}

const GalleryDetailPage = ({ gallery, serveGalleriesFromCdn }: GalleryDetailPageProps) => {
  return (
    <>
      <SEO
        title={gallery.data.title}
        description={gallery.data.summary}
        images={gallery.data.images.map((img) => img.url)}
      />
      <main>
        <div className="grid py-12 mx-auto">
          <div className="pb-4">
            <h1 className="py-4 font-serif text-5xl font-bold text-center text-transparent md:text-6xl from-titleTextStart to-titleTextEnd bg-gradient-to-r bg-clip-text">
              {gallery.data.title}
            </h1>
            <p className="text-2xl font-bold text-center text-gray-500">
              {dateUtils.formatDate(gallery.data.dateOfWork, dateUtils.DateFormat.MonthYearOnly)}
              {gallery.data.ongoing && (
                <>
                  <NBSP />- Current
                </>
              )}
            </p>
          </div>

          <div className="pb-2">
            <p className="text-xl font-bold text-center text-textScript">{gallery.data.summary}</p>
          </div>

          <div className="pt-2 pb-4 mx-auto">
            <ul className="inline-flex flex-wrap gap-2">
              {gallery.data.tags.map((tag) => (
                <TagLabel key={tag} id={tag} />
              ))}
            </ul>
          </div>
        </div>

        <div className="grid items-center grid-flow-row grid-cols-1 gap-32 p-8 justify-items-center md:p-32">
          <ScrollingGallery
            images={gallery.data.images.map((img) => ({
              url: img.url,
              width: img.width,
              height: img.height,
              title: 'TODO',
            }))}
            serveGalleryFromCdn={serveGalleriesFromCdn}
          />
        </div>
      </main>
    </>
  )
}

export const getStaticPaths = async () => ({
  paths: api.cms.listGalleries().map((gallery) => ({
    params: { slug: gallery.slug },
  })),
  fallback: false,
})

export const getStaticProps: GetStaticProps<GalleryDetailPageProps> = async (context) => ({
  props: {
    gallery: api.cms.getGallery(nextUtils.getSlugFromStaticPropsContext(context)),
    serveGalleriesFromCdn: api.cms.getSiteContent().serveGalleriesFromCdn,
  },
})

export default GalleryDetailPage
