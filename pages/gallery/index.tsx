import React from 'react'
import { GetStaticProps } from 'next'
import { api } from 'src/lib/api'
import { GalleryCard, GalleryCardImage } from 'src/components/GalleryCard'

type GallerySummary = {
  slug: string
  title: string
  year: number
  image: GalleryCardImage
  tags: string[]
}

type GalleryHomeProps = {
  galleries: GallerySummary[]
}
const GalleryHome = ({ galleries }: GalleryHomeProps) => {
  return (
    <>
      <div className="py-12 mx-auto text-center prose prose-2xl text-textScript uppercase font-extrabold font-mono">
        <p>the collection of my collections</p>
      </div>
      <br />
      <br />
      <div className="items-center grid grid-cols-1 lg:grid-cols-2 justify-items-center gap-32">
        {galleries.map((gallery) => (
          <GalleryCard
            key={gallery.slug}
            slug={gallery.slug}
            title={gallery.title}
            year={gallery.year}
            image={gallery.image}
            tags={gallery.tags}
            smallBackgroundImageStack={true}
          />
        ))}
      </div>
    </>
  )
}

export const getStaticProps: GetStaticProps<GalleryHomeProps> = async () => ({
  props: {
    galleries: api.cms
      .listGalleries()
      .filter((rawGallery) => rawGallery.slug !== api.cms.constant.FeaturedGallery)
      .map((gallery) => ({
        slug: gallery.slug,
        title: gallery.data.title,
        year: new Date(gallery.data.dateOfWork).getFullYear(),
        image: {
          fileName: 'TODO',
          url: gallery.data.images[0].url,
          height: gallery.data.images[0].height,
          width: gallery.data.images[0].width,
        },
        tags: gallery.data.tags,
      })),
  },
})

export default GalleryHome
