// eslint-disable-next-line @typescript-eslint/no-var-requires
const colors = require('tailwindcss/colors')
// eslint-disable-next-line @typescript-eslint/no-var-requires
const plugin = require('tailwindcss/plugin')
// eslint-disable-next-line @typescript-eslint/no-var-requires
const defaultTheme = require('tailwindcss/defaultTheme')

/** @type {import("@types/tailwindcss/tailwind-config").TailwindConfig } */
module.exports = {
  mode: 'jit',
  purge: ['./pages/**/*.{js,ts,jsx,tsx}', './src/**/*.{js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    screens: {
      md: '720px',
      ...defaultTheme.screens,
    },
    extend: {
      colors: {
        accent: '#D8D8C7',
        overlayg1: '#00000088',
        overlayg2: '#FFFFFF44',

        background: '#B3E2FE',
        titleTextStart: '#01A0FF',
        titleTextEnd: '#B3E2FE',
        textScript: '#000000',
        complement: {
          50: '#FFE3AF',
          100: '#FFDBAF',
          200: '#FFB964',
          300: '#FF9B20',
          400: '#FF8C00',
        },
      },
      margin: {
        '-menu': '-28rem',
      },
      width: {
        '9/10': '92%',
      },
      height: {
        '9/10': '92%',
      },
      zIndex: {
        100: '100',
      },
      typography: {
        DEFAULT: {
          css: {
            a: {
              color: '#FFFFFF',
              '&:hover': {
                color: '#D8D8C7',
              },
            },
            ul: {
              'li::before': {
                'background-color': colors.black,
              },
            },
          },
        },
      },
      scale: {
        115: '1.15',
      },
      rotate: {
        4: '4deg',
        '-4': '-4deg',
      },
      spacing: {
        max2: '26rem',
        max3: '40rem',
      },
      animation: {
        'pulse-slow': 'pulse 3s infinite',
      },
    },
  },
  variants: {
    extend: {
      transform: ['hover', 'focus', 'group-hover'],
      rotate: ['odd', 'even', 'group-hover', '3n'],
      translate: ['odd', 'even', 'group-hover', '3n'],
      scale: ['hover', 'focus', 'group-hover'],
      inset: ['first', 'even', 'odd', '3n'],
    },
  },
  plugins: [
    require('@tailwindcss/typography'),
    require('@tailwindcss/aspect-ratio'),
    plugin(function ({ addVariant, e }) {
      addVariant('3n', ({ modifySelectors, separator }) => {
        modifySelectors(({ className }) => {
          return `.${e(`3n${separator}${className}`)}:nth-child(3n+0)`
        })
      })
    }),
  ],
}
