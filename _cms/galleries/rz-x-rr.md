---
title: RZ x RR
draft: false
dateOfWork: 2021-09-24T03:19:10.227Z
ongoing: false
summary: An exercise in portraiture -
tags:
  - portrait
  - film
images:
  - /static/cms/RodrigoxRR_Edits__RVR4944_1.jpg
  - /static/cms/RodrigoxRR_Edits__RVR4951_2.jpg
  - /static/cms/RodrigoxRR_Edits__RVR4957_3.jpg
  - /static/cms/RodrigoxRR_Edits__RVR4964_4.jpg
  - /static/cms/RodrigoxRR_Edits__RVR4971_5.jpg
  - /static/cms/RodrigoxRR_Edits__RVR5002_6.jpg
---
