---
title: Featured
draft: false
dateOfWork: 2019-12-26T01:36:45.915Z
ongoing: true
summary: a collection of my most meaningful work
tags:
  - film
  - digital
images:
  - /static/cms/Mik-RVR03529.jpg
  - /static/cms/Washmon_RR_01.jpg
  - /static/cms/EmilyBen_27.jpg
  - /static/cms/Mik-RVR03635.jpg
  - /static/cms/RodrigoxRR_Edits__RVR4944_1.jpg
  - /static/cms/Washmon_RR_16.jpg
  - /static/cms/PR-RVR03411.jpg
  - /static/cms/EmilyBen_09.jpg
  - /static/cms/stevenandtito.png
---
