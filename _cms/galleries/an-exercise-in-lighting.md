---
title: "An Exercise In: Lighting"
draft: false
dateOfWork: 2021-10-01T20:43:19.761Z
ongoing: false
summary: a lighting project for NYU.
tags:
  - digital
  - parsons
  - lighting
images:
  - /static/cms/RVR_X_Lighting_01.jpg
  - /static/cms/RVR_X_Lighting_02.jpg
  - /static/cms/RVR_X_Lighting_03.jpg
  - /static/cms/RVR_X_Lighting_04.jpg
  - /static/cms/RVR_X_Lighting_05.jpg
---
