---
title: Family Gallery
draft: false
dateOfWork: 1994-10-09T20:20:00.000Z
ongoing: false
summary: "Two photos of my family: Steven and Tito together and Tito snuggly in
  a blanket."
tags:
  - tito
  - steven
images:
  - /static/cms/titoinblanket.png
  - /static/cms/stevenandtito.png
---
