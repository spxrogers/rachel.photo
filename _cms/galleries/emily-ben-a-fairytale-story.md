---
title: Emily & Ben | A FAIRYTALE STORY
draft: false
dateOfWork: 2021-05-20T02:49:16.443Z
ongoing: false
summary: |-
  a young couple. 
  madly in love. 
  stronger than ink. 
  more tightly bound than a book.
tags:
  - film
  - digital
  - engagement
images:
  - /static/cms/EmilyBen_27.jpg
  - /static/cms/EmilyBen_01.jpg
  - /static/cms/EmilyBen_02.jpg
  - /static/cms/EmilyBen_03.jpg
  - /static/cms/EmilyBen_04.jpg
  - /static/cms/EmilyBen_05.jpg
  - /static/cms/EmilyBen_06.jpg
  - /static/cms/EmilyBen_07.jpg
  - /static/cms/EmilyBen_08.jpg
  - /static/cms/EmilyBen_09.jpg
  - /static/cms/EmilyBen_10.jpg
  - /static/cms/EmilyBen_11.jpg
  - /static/cms/EmilyBen_12.jpg
  - /static/cms/EmilyBen_13.jpg
  - /static/cms/EmilyBen_14.jpg
  - /static/cms/EmilyBen_15.jpg
  - /static/cms/EmilyBen_16.jpg
  - /static/cms/EmilyBen_17.jpg
  - /static/cms/EmilyBen_18.jpg
  - /static/cms/EmilyBen_19.jpg
  - /static/cms/EmilyBen_20.jpg
  - /static/cms/EmilyBen_21.jpg
  - /static/cms/EmilyBen_22.jpg
  - /static/cms/EmilyBen_23.jpg
  - /static/cms/EmilyBen_24.jpg
  - /static/cms/EmilyBen_25.jpg
  - /static/cms/EmilyBen_26.jpg
  - /static/cms/EmilyBen_27.jpg
---
