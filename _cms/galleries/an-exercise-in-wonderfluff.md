---
title: "An Exercise In: Wonderfluff"
draft: false
dateOfWork: 2021-10-05T20:48:12.579Z
ongoing: false
summary: a Wonderfluff picnic
tags:
  - digital
  - parsons
  - wonderfluff
  - centralpark
images:
  - /static/cms/RR_Gram_00.jpg
  - /static/cms/RR_Gram_01.jpg
  - /static/cms/RR_Gram_02.jpg
  - /static/cms/RR_Gram_03.jpg
  - /static/cms/RR_Gram_04.jpg
  - /static/cms/RR_Gram_05.jpg
  - /static/cms/RR_Gram_06.jpg
  - /static/cms/RR_Gram_07.jpg
  - /static/cms/RR_Gram_08.jpg
  - /static/cms/RR_Gram_09.jpg
  - /static/cms/RR_Gram_10.jpg
  - /static/cms/RR_Gram_11.jpg
---
