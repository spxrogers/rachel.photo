---
title: A Study of Mikhaila
draft: false
dateOfWork: 2021-07-02T03:27:20.051Z
ongoing: true
summary: capturing a muse
tags:
  - film
  - digital
  - portrait
images:
  - /static/cms/Mik-RVR03515.jpg
  - /static/cms/Mik-000085690006.jpg
  - /static/cms/Mik-000085690008.jpg
  - /static/cms/Mik-000085690016.jpg
  - /static/cms/Mik-000092390003.jpg
  - /static/cms/Mik-000092390008.jpg
  - /static/cms/Mik-000092390010.jpg
  - /static/cms/Mik-000092390016.jpg
  - /static/cms/Mik-000093160003.jpg
  - /static/cms/Mik-000093160017.jpg
  - /static/cms/Mik-000093160026.jpg
  - /static/cms/Mik-RVR03499.jpg
  - /static/cms/Mik-RVR03515.jpg
  - /static/cms/Mik-RVR03525-2.jpg
  - /static/cms/Mik-RVR03529.jpg
  - /static/cms/Mik-RVR03532.jpg
  - /static/cms/Mik-RVR03543.jpg
  - /static/cms/Mik-RVR03549.jpg
  - /static/cms/Mik-RVR03553.jpg
  - /static/cms/Mik-RVR03561.jpg
  - /static/cms/Mik-RVR03603.jpg
  - /static/cms/Mik-RVR03635.jpg
  - /static/cms/Mik-RVR03649.jpg
---
