---
title: Site Content
serveGalleriesFromCdn: true
homePageParagraph: a self-taught photographer living a life through a lens looking up.
aboutTitle: About me
aboutContent: |-
  I'm a 27 year-old self-taught photographer based in NYC.

  My favorite dog is Tito.
---
