---
title: Hello rachel.photo
draft: true
publishDate: 2021-10-09T19:06:36.578Z
author: Steven Rogers
summary: hello, this is Steven and I am testing the NetlifyCMS publish workflow
  for a blob post.
tags:
  - blog
  - blog
  - blog
  - blog
  - blog
  - blog
  - blog
mainImage: /static/cms/pug404.jpg
---
Steven Rogers here, not Rachel -- I'm sorry to disappoint. I am using this test blog page to tweak the style and formatting for the various types of text fields and make adjustments so that headings, bold, italics, lists, and friends all render not-so-awfully.

:+1:

# Heading 1

## Heading 2

### Heading 3

#### Heading 4

##### Heading 5

###### Heading 6

Some text with some **Bold text** in the middle.

Some text with some *Italic text* in the middle.

Some text with some  `Code text` in the middle.

> Quote text

* Unordered

  * Nested

    * More
* List

1. Ordered

   1. Nested

      1. More
2. List

```javascript
const isBlogPost = () => true;
```

cheers,
S
