/** @type {import('next').NextConfig} */
module.exports = {
  reactStrictMode: true,
  images: {
    domains: ['spxrogers.com', 'srogers.net', 'glcdn.githack.com', 'images.ctfassets.net', 'downloads.ctfassets.net'],
  },
  generateBuildId: () => 'build',
}
