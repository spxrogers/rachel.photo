import React from 'react'
import unified from 'unified'
import markdownParser from 'remark-parse'
// @ts-ignore
import markdownToJsx from 'remark-react'

export type MarkdownProps = {
  mdString: string
}

export const Markdown = ({ mdString }: MarkdownProps) => (
  <div className="prose">
    <>{unified().use(markdownParser).use(markdownToJsx).processSync(mdString).result}</>
  </div>
)
