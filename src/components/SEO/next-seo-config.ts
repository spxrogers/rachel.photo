import { DefaultSeoProps } from 'next-seo'

const title = 'Rachel Rogers | Photographer'
const description = 'A photography gallery and portfolio from the select works of Rachel Rogers'
const siteUrl = 'https://rachel.photo'

const seoConfig: DefaultSeoProps = {
  titleTemplate: `%s | RR`,
  defaultTitle: title,
  description: description,
  canonical: siteUrl,
  openGraph: {
    type: 'website',
    locale: 'en_US',
    url: siteUrl,
    title: title,
    description: description,
    images: [
      {
        url: 'https://rachel.photo/static/img/rachelinstaprofilephoto_20211003.jpg',
        width: 1920,
        height: 1280,
        alt: 'Open Graph Image',
      },
    ],
  },
  twitter: {
    handle: '@mrsrachrogers',
    site: siteUrl,
    cardType: 'summary_large_image',
  },
}

export const getSeoConfig = () => ({ ...seoConfig })
