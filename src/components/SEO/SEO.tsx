import React from 'react'
import { DefaultSeo, NextSeo, NextSeoProps } from 'next-seo'
import { getSeoConfig } from './next-seo-config'

export type SeoProps = {
  title?: string
  description?: string
  images?: string[]
}

const DefaultSEO = () => <DefaultSeo {...getSeoConfig()} />

export const SEO = ({ title, description, images }: SeoProps) => {
  const props: NextSeoProps = {}

  if (title) {
    props.title = title
    props.openGraph = {}
    props.openGraph.title = title
  }

  if (description) {
    props.description = description
    props.openGraph = props.openGraph ?? {}
    props.openGraph.description = description
  }

  if (images) {
    props.openGraph = props.openGraph ?? {}
    props.openGraph.images = images.map((imageUrl) => ({ url: imageUrl }))
  }

  return <NextSeo {...props} />
}

SEO.Default = DefaultSEO
