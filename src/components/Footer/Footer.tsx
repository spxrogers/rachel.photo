import React from 'react'
import { Copyright } from 'src/components/Copyright'
import { dateUtils } from 'src/lib/utils'
import { Icon, IconType } from 'src/components/Icon'
import { NBSP } from 'src/components/NBSP'
import { Link } from 'src/components/Link'

export const Footer = () => (
  <footer>
    <div className="py-12 mx-auto md:flex md:items-center md:justify-between">
      <div className="flex justify-center space-x-6 md:order-2">
        <Link href={'https://instagram.com/mrsrachelvrogers'} newTab={true}>
          <div className="w-6 h-6 text-gray-400 hover:text-accent">
            <span className="sr-only">Instagram</span>
            <Icon type={IconType.Instagram} />
          </div>
        </Link>
      </div>
      <div className="mt-8 md:mt-0 md:order-1">
        <p className="text-base text-center text-gray-500">
          <Copyright />
          <NBSP />
          1994-{dateUtils.currentYear()} Rachel Rogers. All rights reserved.
        </p>
      </div>
    </div>
  </footer>
)
