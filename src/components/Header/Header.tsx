import React, { useState } from 'react'
import { Menu, Transition } from '@headlessui/react'
import { Link } from 'src/components/Link'
import { Icon, IconType } from 'src/components/Icon'
import { useOnRouteChange } from 'src/hooks'

type NavProps = {
  isNavOpen: boolean
  openNav: () => void
  closeNav: () => void
}

export const Header = () => {
  const [isNavOpen, setIsNavOpen] = useState(false)
  const navProps: NavProps = {
    isNavOpen,
    openNav: () => setIsNavOpen(true),
    closeNav: () => setIsNavOpen(false),
  }

  useOnRouteChange(navProps.closeNav)

  return (
    <div className="sticky top-0 pt-4 mx-auto z-100 max-w-7xl bg-gradient-to-b from-background">
      <div className="flex items-center justify-between py-6">
        <RR />
        <MobileNav {...navProps} />
        <DesktopNav {...navProps} />
      </div>
    </div>
  )
}

const RR = () => (
  <Link href={'/'}>
    <span className="font-mono text-xl font-black cursor-pointer text-textScript">RR</span>
  </Link>
)

const MobileNav = ({ isNavOpen, openNav, closeNav }: NavProps) => (
  <>
    <div className="-my-2 -mr-2 md:hidden">
      <button
        type="button"
        onClick={() => (isNavOpen ? closeNav() : openNav())}
        className="inline-flex items-center justify-center p-2 bg-gray-900 rounded-md bg-opacity-10 text-accent hover:text-white hover:bg-gray-800 focus:outline-none"
      >
        <span className="sr-only">Open menu</span>
        <Icon type={IconType.HamburgerMenu} />
      </button>
    </div>
    <Transition
      show={isNavOpen}
      enter="transition ease-out duration-100 transform"
      enterFrom="opacity-0 scale-95"
      enterTo="opacity-100 scale-100"
      leave="transition ease-in duration-75 transform"
      leaveFrom="opacity-100 scale-100"
      leaveTo="opacity-0 scale-95"
    >
      {(ref) => (
        <div ref={ref} className="absolute inset-x-0 p-2 transition origin-top-right transform z-100 top-5 md:hidden">
          <div className="bg-gray-900 divide-y-2 rounded-lg shadow-lg ring-1 ring-black ring-opacity-5 divide-gray-50">
            <div className="relative p-5">
              <div className="absolute top-2 right-2">
                <button
                  type="button"
                  onClick={() => (isNavOpen ? closeNav() : openNav())}
                  className="inline-flex items-center justify-center p-2 text-gray-400 bg-gray-900 rounded-md hover:text-accent hover:bg-gray-800 focus:outline-none"
                >
                  <span className="sr-only">Close menu</span>
                  <Icon type={IconType.Close} />
                </button>
              </div>

              <div className="w-11/12">
                <nav className="grid gap-y-8">
                  {navConfig
                    .filter((navItem) => navItem.navTypes.includes(NavType.Mobile))
                    .map((navItem) => (
                      <Link href={navItem.link} newTab={navItem.openLinkInNewTab} key={navItem.id}>
                        <span className="flex items-center p-3 -m-3 rounded-md cursor-pointer hover:bg-gray-800">
                          <Icon type={navItem.icon} />
                          <span className="ml-3 text-base font-medium text-white">{navItem.title}</span>
                          {navItem.openLinkInNewTab && <Icon type={IconType.ExternalLink} />}
                        </span>
                      </Link>
                    ))}

                  <Link href={contactMe.url}>
                    <div className="flex items-center p-3 -m-3 rounded-md hover:bg-gray-800">
                      <Icon type={contactMe.icon} />
                      <span className="ml-3 text-base font-medium text-white">{contactMe.title}</span>
                    </div>
                  </Link>
                </nav>
              </div>
            </div>
          </div>
        </div>
      )}
    </Transition>
  </>
)

const DesktopNav = ({ isNavOpen, openNav, closeNav }: NavProps) => (
  <>
    <div className="hidden md:flex">
      <Link href="/featured">
        <span className="inline-flex items-center ml-4 text-base font-medium text-textScript outline-none cursor-pointer group hover:text-gray-600">
          Featured Work
        </span>
      </Link>
      <Menu>
        {() => (
          <>
            <Menu.Button
              className="inline-flex items-center ml-4 text-base font-medium text-textScript outline-none group hover:text-gray-600 focus:outline-none"
              onClick={() => (isNavOpen ? closeNav() : openNav())}
            >
              <span>More</span>
              {isNavOpen ? <Icon type={IconType.ChevronUp} /> : <Icon type={IconType.ChevronDown} />}
            </Menu.Button>
            <Transition
              show={isNavOpen}
              enter="transition ease-out duration-80 transform"
              enterFrom="opacity-0 scale-90"
              enterTo="opacity-100 scale-100"
              leave="transition ease-in duration-80transform"
              leaveFrom="opacity-100 scale-100"
              leaveTo="opacity-0 scale-90"
            >
              <Menu.Items static as={React.Fragment}>
                <div className="absolute z-50 mt-12 outline-none -ml-menu">
                  <div className="w-screen max-w-md overflow-hidden rounded-lg shadow-lg ring-1 ring-black ring-opacity-5">
                    <div className="relative grid gap-8 px-8 py-8 bg-gray-900">
                      {navConfig
                        .filter((navItem) => navItem.navTypes.includes(NavType.Desktop))
                        .map((navItem) => (
                          <Menu.Item key={navItem.id}>
                            <Link href={navItem.link} newTab={navItem.openLinkInNewTab}>
                              <div className="flex items-start p-3 -m-3 rounded-lg hover:bg-gray-800">
                                <Icon type={navItem.icon} />
                                <div className="ml-4">
                                  <p className="inline-flex text-base font-medium text-white">
                                    {navItem.title}
                                    {navItem.openLinkInNewTab && <Icon type={IconType.ExternalLink} />}
                                  </p>
                                  <p className="mt-1 text-sm text-gray-200">{navItem.subtitle}</p>
                                </div>
                              </div>
                            </Link>
                          </Menu.Item>
                        ))}
                    </div>
                    <Menu.Item>
                      <div className="px-5 py-5 space-y-6 bg-gray-800 hover:bg-gray-700 sm:flex sm:space-y-0 sm:space-x-10 sm:px-8">
                        <div className="flow-root w-full">
                          <Link href={contactMe.url}>
                            <div className="flex items-center p-3 -m-3 text-base font-medium text-white rounded-md">
                              <Icon type={contactMe.icon} />
                              <span className="ml-3">{contactMe.title}</span>
                            </div>
                          </Link>
                        </div>
                      </div>
                    </Menu.Item>
                  </div>
                </div>
              </Menu.Items>
            </Transition>
          </>
        )}
      </Menu>
    </div>
  </>
)

enum NavType {
  Desktop,
  Mobile,
}

type NavItem = {
  id: number
  navTypes: NavType[]
  icon: IconType
  title: string
  subtitle: string
  link: string
  openLinkInNewTab?: boolean
}

const navConfig: NavItem[] = [
  {
    navTypes: [NavType.Mobile],
    icon: IconType.Camera,
    title: 'Featured Work',
    subtitle: 'Selected works',
    link: '/featured',
  },
  {
    icon: IconType.User,
    title: 'About me',
    subtitle: 'Biography and my story',
    link: '/about',
  },
  {
    icon: IconType.Camera,
    title: 'Galleries',
    subtitle: 'A lookbook of my collections',
    link: '/gallery',
  },
  {
    icon: IconType.Book,
    title: 'Blog',
    subtitle: 'Written word by Rachel',
    link: '/blog',
  },
  {
    icon: IconType.RssFeed,
    title: 'Photo Archive',
    subtitle: 'Stream of conscious of my body of work',
    link: '/archive?view=grid',
  },
  {
    icon: IconType.Group,
    title: 'Husband',
    subtitle: "My husband's work",
    link: 'http://spxrogers.com',
    openLinkInNewTab: true,
  },
].map((item) => ({
  ...item,
  id: Math.random(),
  navTypes: item.navTypes ?? [NavType.Desktop, NavType.Mobile],
}))

const contactMe = {
  icon: IconType.Inbox,
  title: 'Contact me',
  url: '/contact',
}
