import React from 'react'
import NextImage, { ImageProps as NextImageProps } from 'next/image'

export type ImageProps = NextImageProps & {
  lazyLoad?: boolean
}

export const Image = (props: ImageProps) => <NextImage {...props} priority={props.lazyLoad ?? true} />
