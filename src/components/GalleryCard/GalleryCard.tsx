import React from 'react'
import { NBSP } from 'src/components/NBSP'
import { Image } from 'src/components/Image'
import { Link } from 'src/components/Link'
import { collectionUtils } from 'src/lib/utils'

export type GalleryCardImage = {
  fileName: string
  url: string
  height: number
  width: number
}

export type GalleryCardProps = {
  title: string
  year: number
  slug: string
  image: GalleryCardImage
  tags: string[]
  smallBackgroundImageStack: boolean
}

export const GalleryCard = ({
  title,
  year,
  slug,
  image,
  tags,
  smallBackgroundImageStack = false,
}: GalleryCardProps) => {
  return (
    <>
      <Container>
        <div className="z-50 max-w-2xl p-2 bg-gray-200 rounded-sm shadow-2xl transform group-hover:scale-105 transition-transform duration-500 ease-in-out">
          <Link href={`/${slug}/`}>
            <div className="relative cursor-pointer max-w-max">
              <div className="relative flex">
                <Image src={image.url} alt={'TODO'} width={image.width} height={image.height} layout="intrinsic" />
              </div>

              <div className="absolute bottom-0 w-full h-full">
                <div className="z-50 items-end h-full p-4 from-overlayg1 to-overlayg2 bg-gradient-to-t grid grid-flow-row">
                  <div>
                    <h2 className="text-xl font-extrabold uppercase">{title}</h2>
                    <div>
                      <span className="flex font-semibold uppercase text-accent">
                        <ul className="inline-flex flex-wrap gap-1">
                          <li>
                            {year}
                            <NBSP />-<NBSP />
                          </li>
                          {collectionUtils.joinCollection(
                            tags.map((tag) => <li key={tag}>{tag}</li>),
                            ' | '
                          )}
                        </ul>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </Link>
        </div>
        <BackgroundPictureOne smallBackgroundImageStack={smallBackgroundImageStack} />
        <BackgroundPictureTwo smallBackgroundImageStack={smallBackgroundImageStack} />
      </Container>
    </>
  )
}

const Container = ({ children }: React.PropsWithChildren<Record<string, unknown>>) => (
  // TS doesn't find the <container/> tag -- meh ¯\_(ツ)_/¯.
  // @ts-ignore
  <container className={'flex items-center justify-center group'}>{children}</container>
)

const BackgroundPictureOne = ({ smallBackgroundImageStack }: BackgroundPictureProps) => (
  <div className="absolute z-10 border-8 border-gray-300 rounded-sm shadow-xl transform rotate-2 group-hover:rotate-4 scale-100 sm:scale-110 group-hover:scale-115 transition-all duration-500 ease-in-out">
    {smallBackgroundImageStack ? (
      <div className="hidden bg-yellow-900 sm:block opacity-90 md:w-max2 md:h-max2" />
    ) : (
      <div className="hidden bg-yellow-900 sm:block opacity-90 md:w-max3 md:h-max2" />
    )}
  </div>
)

const BackgroundPictureTwo = ({ smallBackgroundImageStack }: BackgroundPictureProps) => (
  <div className="absolute z-0 border-8 border-gray-300 rounded-sm shadow-xl transform -rotate-3 group-hover:-rotate-4 scale-100 sm:scale-110 group-hover:scale-115 transition-all duration-500 ease-in-out">
    {smallBackgroundImageStack ? (
      <div className="hidden bg-yellow-900 md:block opacity-90 md:w-max2 md:h-max2" />
    ) : (
      <div className="hidden bg-yellow-900 md:block opacity-90 md:w-max3 md:h-max2" />
    )}
  </div>
)

type BackgroundPictureProps = {
  smallBackgroundImageStack: boolean
}
