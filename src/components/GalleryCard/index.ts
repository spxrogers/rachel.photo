export { GalleryCard } from './GalleryCard'
export type { GalleryCardProps, GalleryCardImage } from './GalleryCard'
