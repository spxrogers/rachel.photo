import React from 'react'

export type TagLabelProps = {
  id: string
}

export const TagLabel = ({ id }: TagLabelProps) => (
  <li key={id} className="uppercase text-complement-50 bg-complement-400 text-sm rounded-sm px-2.5 py-1.5">
    {id}
  </li>
)
