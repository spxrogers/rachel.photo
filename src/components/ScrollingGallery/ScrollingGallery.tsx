import React from 'react'
import { Image } from 'src/components/Image'

export type ScrollingGalleryImage = {
  url: string
  title: string
  width: number
  height: number
}

export type ScrollingGalleryProps = {
  images: ScrollingGalleryImage[]
  serveGalleryFromCdn?: boolean
  lazyLoadImages?: boolean
}

export const ScrollingGallery = ({
  images,
  serveGalleryFromCdn = false,
  lazyLoadImages = false,
}: ScrollingGalleryProps) => (
  <>
    {images.map((image) => (
      <div
        key={`${image.url}-${Math.random()}`}
        className="sticky max-w-3xl p-1 transform bg-gray-100 rounded-sm shadow-2xl md:p-2 even:-rotate-2 rotate-1 3n:rotate-4 even:top-32 top-28 first:top-24"
      >
        <Image
          src={serveGalleryFromCdn ? routeImageThroughGitHackCdn(image.url) : image.url}
          alt={'TODO'}
          width={image.width}
          height={image.height}
          lazyLoad={lazyLoadImages}
          layout="intrinsic"
          className="z-50"
        />
        <div className="absolute z-0 w-5/6 bg-yellow-900 animate-pulse-slow h-5/6 md:h-9/10 md:w-9/10 top-4 left-4" />
      </div>
    ))}
  </>
)

// Route photos thru http://raw.githack.com/ -- caches the media on CloudFare's CDN.
// Example:
// -> Input: /static/cms/RodrigoxRR_Edits__RVR4944_1.jpg
// => Output: https://glcdn.githack.com/spxrogers/rachel.photo/-/raw/main/public/static/cms/RodrigoxRR_Edits__RVR4944_1.jpg?min=1
const routeImageThroughGitHackCdn = (url: string) =>
  `https://glcdn.githack.com/spxrogers/rachel.photo/-/raw/main/public${url}?min=1`
