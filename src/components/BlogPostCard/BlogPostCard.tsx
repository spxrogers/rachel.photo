import React from 'react'
import { Link } from 'src/components/Link'
import { Image } from 'src/components/Image'
import { dateUtils } from 'src/lib/utils'

export type BlogPostCardProps = {
  date: string
  slug: string
  title: string
  description: string
  coverImageUrl: string
  coverImageAlt: string
}

export const BlogPostCard = ({ date, description, coverImageAlt, coverImageUrl, slug, title }: BlogPostCardProps) => (
  <>
    <Link href={`/blog/${slug}/`}>
      <div className="justify-start max-w-2xl p-4 m-8 bg-gray-700 cursor-pointer grid grid-flow-row md:grid-flow-col bg-opacity-10 rounded-md transform hover:scale-105 transition-transform duration-500 ease-in-out">
        <div className="relative w-full md:w-48 h-48">
          <Image src={coverImageUrl} alt={coverImageAlt} layout="fill" objectFit="cover" className="rounded-sm" />
        </div>
        <div className="w-full px-4 grid grid-flow-row gap-0">
          <h2 className="font-serif text-3xl font-bold text-textScript">{title}</h2>
          <p className="text-lg text-gray-700">{description}</p>
          <p className="text-sm text-gray-900">
            Published on {dateUtils.formatDate(date, dateUtils.DateFormat.IncludeDay)}
          </p>
        </div>
      </div>
    </Link>
  </>
)
