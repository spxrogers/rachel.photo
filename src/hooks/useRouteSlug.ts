import { useRouter } from 'next/router'

export const useRouteSlug = () => {
  const router = useRouter()
  return (router?.query?.slug ?? '') as string
}
