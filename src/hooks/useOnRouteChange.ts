import { useEffect } from 'react'
import { useRouter } from 'next/router'

export const useOnRouteChange = (onRouteChangeAction: () => void) => {
  const router = useRouter()
  useEffect(() => {
    router.events.on('routeChangeStart', onRouteChangeAction)
    return () => {
      router.events.off('routeChangeStart', onRouteChangeAction)
    }
  }, [])
}
