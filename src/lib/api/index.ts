import { cms } from './cms'

export type { SiteContent, GalleryPage, BlogPage } from './cms'

export const api = {
  cms: cms,
}
