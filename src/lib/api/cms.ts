import path from 'path'
import fs from 'fs'
import matter from 'gray-matter'
import sizeOf from 'image-size'

const FeaturedGallery = 'featured'

const getSiteContent = (): SiteContent =>
  getAllFileContent('site-content').map((rawMetadata) => rawMetadata.metadata as SiteContent)[0]

const listGalleries = (): GalleryPage[] =>
  getAllFileContent('galleries')
    .map((rawGallery) => ({
      slug: rawGallery.slug,
      data: rawGallery.metadata as GalleryMetadata,
    }))
    .map((galleryMetadata) => ({
      slug: galleryMetadata.slug,
      data: {
        ...galleryMetadata.data,
        dateOfWork: dateString(galleryMetadata.data.dateOfWork),
        images: galleryMetadata.data.images.map((image) => {
          const imgSize = getImageDimensions(`public/${image}`)
          return {
            url: image,
            width: imgSize.width,
            height: imgSize.height,
          }
        }),
      },
    }))
    .slice()
    .sort((lhs, rhs) => new Date(rhs.data.dateOfWork).getTime() - new Date(lhs.data.dateOfWork).getTime())

const getFeaturedGallery = (): GalleryPage => getGallery(FeaturedGallery)

const getGallery = (slug: string): GalleryPage =>
  listGalleries().find((gallery) => gallery.slug === slug) as GalleryPage

const getAllImages = () =>
  listGalleries()
    .map((gallery) => gallery.data.images)
    .flatMap((images) => images as CmsImage[])
    .sort(() => 0.5 - Math.random())

const listBlogPosts = (): BlogPage[] =>
  getAllFileContent('blog')
    .map((rawBlog) => ({
      slug: rawBlog.slug,
      markdownContent: rawBlog.markdownContent,
      data: rawBlog.metadata as BlogMetaData,
    }))
    .map((blogMetadata) => {
      const imgSize = getImageDimensions(`public/${blogMetadata.data.mainImage}`)
      return {
        slug: blogMetadata.slug,
        markdownContent: blogMetadata.markdownContent,
        data: {
          ...blogMetadata.data,
          publishDate: dateString(blogMetadata.data.publishDate),
          mainImage: {
            url: blogMetadata.data.mainImage,
            width: imgSize.width,
            height: imgSize.height,
          },
        },
      }
    })
    .slice()
    .sort((lhs, rhs) => new Date(rhs.data.publishDate).getTime() - new Date(lhs.data.publishDate).getTime())

const getBlogPost = (slug: string): BlogPage => listBlogPosts().find((blog) => blog.slug === slug) as BlogPage

export const cms = {
  getSiteContent: getSiteContent,
  listGalleries: listGalleries,
  getFeaturedGallery: getFeaturedGallery,
  getGallery: getGallery,
  getAllImages: getAllImages,
  listBlogPosts: listBlogPosts,
  getBlogPost: getBlogPost,
  constant: {
    FeaturedGallery: FeaturedGallery,
  },
}

const getAllFileContent = (dir: string) => {
  const fullDir = path.join(process.cwd(), '_cms', dir)

  return fs
    .readdirSync(fullDir)
    .map((fileName) => ({
      slug: fileName.replace(/\.md$/, ''),
      fullFilePath: path.join(fullDir, fileName),
    }))
    .map(({ slug, fullFilePath }) => ({
      slug,
      fileContent: fs.readFileSync(fullFilePath, 'utf8'),
    }))
    .map(({ slug, fileContent }) => ({
      slug,
      parsedMarkdown: matter(fileContent),
    }))
    .map(({ slug, parsedMarkdown }) => ({
      slug,
      metadata: parsedMarkdown.data,
      markdownContent: parsedMarkdown.content,
    }))
}

export type SiteContent = {
  serveGalleriesFromCdn: boolean
  homePageParagraph: string
  aboutTitle: string
  aboutContent: string
}

export type GalleryPage = {
  slug: string
  data: GalleryData
}

export type BlogPage = {
  slug: string
  data: BlogData
  markdownContent: string
}

type GalleryCommonData = {
  title: string
  draft: boolean
  ongoing: boolean
  summary: string
  tags: string[]
}

type GalleryMetadata = GalleryCommonData & {
  dateOfWork: Date
  images: string[]
}

type GalleryData = GalleryCommonData & {
  dateOfWork: string
  images: CmsImage[]
}

type BlogCommonData = {
  title: string
  draft: boolean
  author: string
  summary: string
  tags: string[]
}

type BlogMetaData = BlogCommonData & {
  publishDate: Date
  mainImage: string
}

type BlogData = BlogCommonData & {
  publishDate: string
  mainImage: CmsImage
}

type CmsImage = {
  url: string
  width: number
  height: number
}

const dateString = (date: Date) => date.toISOString()

const getImageDimensions = (filePath: string) => {
  const imgSize = sizeOf(filePath)
  return {
    width: imgSize.width ?? 9001,
    height: imgSize.height ?? 9001,
  }
}
