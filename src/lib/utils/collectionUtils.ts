// eslint-disable-next-line @typescript-eslint/no-explicit-any
const joinCollection = (collection: any[], separator: any): any[] =>
  collection?.reduce((prev, curr) => (prev === null ? [curr] : [...prev, separator, curr]), null) ?? []

export const collectionUtils = {
  joinCollection: joinCollection,
}
