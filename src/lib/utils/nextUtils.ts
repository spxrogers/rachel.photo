import { GetStaticPropsContext } from 'next'

const getSlugFromStaticPropsContext = (ctx: GetStaticPropsContext): string => (ctx?.params?.slug as string) || ''

export const nextUtils = {
  getSlugFromStaticPropsContext: getSlugFromStaticPropsContext,
}
