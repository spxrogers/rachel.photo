const currentYear = () => `${new Date().getFullYear()}`

const numYearsSince = (fromYear: number) => new Date().getFullYear() - fromYear

enum DateFormat {
  MonthYearOnly = 'MonthYearOnly',
  IncludeDay = 'IncludeDay',
  IncludeDayOfWeek = 'IncludeDayOfWeek',
}

const formatDate = (date: string, format: DateFormat) =>
  new Date(date).toLocaleDateString('en-US', DateFormatOptions[format])

export const dateUtils = {
  currentYear: currentYear,
  numYearsSince: numYearsSince,
  DateFormat: DateFormat,
  formatDate: formatDate,
}

const DateFormatOptions: { [key in DateFormat]: Intl.DateTimeFormatOptions } = {
  [DateFormat.MonthYearOnly]: {
    year: 'numeric',
    month: 'long',
  },
  [DateFormat.IncludeDay]: {
    year: 'numeric',
    month: 'long',
    day: 'numeric',
  },
  [DateFormat.IncludeDayOfWeek]: {
    weekday: 'long',
    year: 'numeric',
    month: 'long',
    day: 'numeric',
  },
}
